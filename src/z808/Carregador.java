/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z808;

import java.awt.SystemColor;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.Stack;

/**
 *
 * @author Weslen
 */
public class Carregador {

    private int AX;
    private int DX;

    private Memoria memoria;
    private Stack<Integer> pilha;

    private boolean flagZero;
    private boolean flagCarry;
    private boolean flagOverFlow;
    private boolean flagParidade;
    private boolean flagInterupcao;
    

    public Carregador() {
        this.memoria = new Memoria();
        this.pilha = new Stack<Integer>();

        this.flagZero = false;
        this.flagCarry = false;
        this.flagInterupcao = false;
        this.flagOverFlow = false;
        this.flagParidade = false;

    }

    public void carrega(String nomeArquivo, int op) throws FileNotFoundException, IOException {

        Scanner scanner = new Scanner(new FileReader(nomeArquivo));
        FileWriter arq = new FileWriter("Saida.txt");
        PrintWriter gravarArq = new PrintWriter(arq);

        int salto = 0;

        if (op == 1) {

            while (scanner.hasNext()) {
                this.zeraBoolean();
                String linha = scanner.nextLine();
                String[] vetorLinha = linha.split(" ");
                if (vetorLinha.length > 1) {
                    salto = verificaOp(vetorLinha);
                } else if (salto != -1) {
                    scanner = junpLine(scanner, nomeArquivo, salto);

                }
                if(this.flagOverFlow){
                    break;
                }

            }

            gravarArq.printf("AX-- %d", this.AX);
            gravarArq.println();
            gravarArq.printf("DX-- %d", this.DX);
            gravarArq.println();
            gravarArq.printf("Carry-- %b", this.flagCarry);
            gravarArq.println();
            gravarArq.printf("Interrupção-- %b", this.flagInterupcao);
            gravarArq.println();
            gravarArq.printf("OverFlow-- %b", this.flagOverFlow);
            gravarArq.println();
            gravarArq.printf("Paridade-- %b", this.flagParidade);
            gravarArq.println();
            gravarArq.printf("Zero-- %b", this.flagZero);
            gravarArq.close();

        } else {
            while (scanner.hasNext()) {
                String linha = scanner.nextLine();
                String[] vetorLinha = linha.split(" ");
                salto = verificaOp(vetorLinha);
                if (salto != -1) {
                    scanner = junpLine(scanner, nomeArquivo, salto);

                }

                gravarArq.printf("AX-- %d", this.AX);
                gravarArq.println();
                gravarArq.printf("DX-- %d", this.DX);
                gravarArq.println();
                gravarArq.printf("Carry-- %b", this.flagCarry);
                gravarArq.println();
                gravarArq.printf("Interrupção-- %b", this.flagInterupcao);
                gravarArq.println();
                gravarArq.printf("OverFlow-- %b", this.flagOverFlow);
                gravarArq.println();
                gravarArq.printf("Paridade-- %b", this.flagParidade);
                gravarArq.println();
                gravarArq.printf("Zero-- %b", this.flagZero);
                gravarArq.println();
                gravarArq.println();
                
                if(this.flagOverFlow){
                    break;
                }
               
                this.zeraBoolean();

            }
            gravarArq.close();

        }
    }

    public Scanner junpLine(Scanner entrada, String nomeDoArquivo, int linha) throws FileNotFoundException {
        int k = 0;

        entrada.close();
        entrada = new Scanner(new FileReader(nomeDoArquivo));
        while (k < linha) {
            String instrucoes = entrada.nextLine();
            String[] vetorLinha = instrucoes.split(" ");
            k = (k + vetorLinha.length) - 1;

        }

        return entrada;
    }

    public int verificaOp(String[] entrada) {
        int valorRecebido;

        if ((entrada[0].equals("8B")) || ((entrada[0].equals("B8")))) {// mov
            valorRecebido = retornaValorRegist(entrada[2]);
            setValor(entrada[1], valorRecebido);
            return -1;
        } else if (entrada[0].equals("03")) {// soma de valores
            valorRecebido = retornaValorRegist(entrada[2]);
            valorRecebido = valorRecebido + retornaValorRegist(entrada[1]);
            this.setExcecoes(valorRecebido, this.retornaValorRegist(entrada[1]), 0);
            setValor(entrada[1], valorRecebido);
            return -1;
        } else if (entrada[0].equals("2B")) {// subtração
            valorRecebido = retornaValorRegist(entrada[2]);
            valorRecebido = retornaValorRegist(entrada[1]) - valorRecebido;
            this.setExcecoes(this.retornaValorRegist(entrada[1]), valorRecebido, 1);
            setValor(entrada[1], valorRecebido);
            return -1;
        } else if (entrada[0].equals("F6")) {// Divisão
            valorRecebido = retornaValorRegist(entrada[2]);
            valorRecebido = retornaValorRegist(entrada[1]) / valorRecebido;
            this.setExcecoes(this.retornaValorRegist(entrada[1]), valorRecebido, 3);
            setValor(entrada[1], valorRecebido);
            return -1;
        } else if (entrada[0].equals("E6")) {// multiplicaçao         

            valorRecebido = retornaValorRegist(entrada[1]);
            if (entrada[1].equals("AX")) {
                valorRecebido = this.DX * valorRecebido;
            }else {
                valorRecebido = this.AX * valorRecebido;
            }
            this.setExcecoes(this.retornaValorRegist(entrada[1]), valorRecebido, 2);
            setValor(entrada[1], valorRecebido);
            return -1;
        } else if (entrada[0].equals("EB")) {// Jump incodicional
            return valorRecebido = Integer.parseInt(entrada[1]);

        } else if (entrada[0].equals("JE")) {// Jump Se ax for negativo
            if (this.AX < 0) {
                return valorRecebido = Integer.parseInt(entrada[1]);
            } else {
                return -1;
            }
        } else if (entrada[0].equals("JNZ")) {// Jump Se ax for 0
            if (this.AX == 0) {
                return valorRecebido = Integer.parseInt(entrada[1]);
            } else {
                return -1;
            }
        } else if (entrada[0].equals("JP")) {// Jump Se ax for positivo
            if (this.AX > 0) {
                return valorRecebido = Integer.parseInt(entrada[1]);
            } else {
                return -1;
            }
        } else if (entrada[0].equals("23")) {// and 
            valorRecebido = retornaValorRegist(entrada[2]);
            valorRecebido = valorRecebido & retornaValorRegist(entrada[1]);
            setValor(entrada[1], valorRecebido);
            return -1;
        } else if (entrada[0].equals("0B")) {// or
            valorRecebido = retornaValorRegist(entrada[2]);
            valorRecebido = valorRecebido | retornaValorRegist(entrada[1]);
            setValor(entrada[1], valorRecebido);
            return -1;
        } else if (entrada[0].equals("33")) {// xor
            valorRecebido = retornaValorRegist(entrada[2]);
            valorRecebido = valorRecebido ^ retornaValorRegist(entrada[1]);
            setValor(entrada[1], valorRecebido);
            return -1;
        } else if (entrada[0].equals("F7")) {// not
            valorRecebido = ~retornaValorRegist(entrada[1]);

            setValor(entrada[1], valorRecebido);
            return -1;
        } else if (entrada[0].equals("58")) {// pop
            if (this.pilha.empty()) {
                setValor(entrada[1], 0);

            } else {
                valorRecebido = this.pilha.pop();
                setValor(entrada[1], valorRecebido);
            }
            return -1;
        } else if (entrada[0].equals("50")) {// push
            valorRecebido = retornaValorRegist(entrada[1]);
            this.pilha.push(valorRecebido);

            return -1;
        } else {
            int pos = Integer.parseInt(entrada[0]);
            int valor = Integer.parseInt(entrada[entrada.length - 1]);
            this.memoria.setMemoria(valor, pos);
            return -1;
        }

    }

    public int retornaValorRegist(String entrada) {

        if ((entrada.equals("AX")) || (entrada.equals("ax"))) {
            return this.AX;
        } else if ((entrada.equals("DX")) || (entrada.equals("dx"))) {
            return this.DX;
        } else {
            int aux = Integer.parseInt(entrada); // posição de memoria deve ser retonada o valor
            return this.memoria.getMemoria(aux);
        }

    }

    public void setValor(String entrada, int valor) {

        if ((entrada.equals("AX")) || (entrada.equals("ax"))) {
            this.AX = valor;
        } else if ((entrada.equals("DX")) || (entrada.equals("dx"))) {
            this.DX = valor;
        } else {
            int aux = Integer.parseInt(entrada);
            this.memoria.setMemoria(valor, aux);
        }

    }

    public void setExcecoes(int resultado) {
        if (resultado == 0) {
            this.flagZero = true;
        } else {
            this.flagZero = false;
        }

    }

    public void setExcecoes(int valorA, int valorB, int operacao) {              // 0-- soma   1-- subtração   2-- Multiplicação  

        int numeroBitsA = this.contaBits(valorA);
        int numeroBitsB = this.contaBits(valorB);

        if (operacao == 0) {

            int resultado = valorA + valorB;

            int numeroBitsResultado = this.contaBits(resultado);

            if ((numeroBitsResultado > numeroBitsA) && (numeroBitsResultado > numeroBitsB)) {
                this.flagCarry = true;
            } else {
                this.flagCarry = false;
            }
            if ((numeroBitsResultado % 2) == 0) {
                this.flagParidade = true;
            } else {
                this.flagParidade = true;
            }
            if (numeroBitsResultado > 16) {
                this.flagOverFlow = true;
            } else {
                this.flagOverFlow = false;
            }

        } else if (operacao == 1) {
            int resultado = valorA - valorB;

            int numeroBitsResultado = this.contaBits(resultado);

            this.setExcecoes(resultado);

            if ((numeroBitsResultado % 2) == 0) {
                this.flagParidade = true;
            } else {
                this.flagParidade = false;
            }

        } else if (operacao == 2) {
            int resultado = valorA * valorB;

            int numeroBitsResultado = this.contaBits(resultado);

            if ((numeroBitsResultado > numeroBitsA) && (numeroBitsResultado > numeroBitsB)) {
                this.flagCarry = true;
            } else {
                this.flagCarry = false;
            }
            if ((numeroBitsResultado % 2) == 0) {
                this.flagParidade = true;
            } else {
                this.flagParidade = true;
            }
            if (numeroBitsResultado > 16) {
                this.flagOverFlow = true;
            } else {
                this.flagOverFlow = false;
            }

        } else if (operacao == 3) {
            int resultado = valorA / valorB;

            int numeroBitsResultado = this.contaBits(resultado);

            if ((numeroBitsResultado > numeroBitsA) && (numeroBitsResultado > numeroBitsB)) {
                this.flagCarry = true;
            } else {
                this.flagCarry = false;
            }
            if ((numeroBitsResultado % 2) == 0) {
                this.flagParidade = true;
            } else {
                this.flagParidade = true;
            }
            if (numeroBitsResultado > 16) {
                this.flagOverFlow = true;
            } else {
                this.flagOverFlow = false;
            }

        }

    }

    private void zeraBoolean() {
        this.flagCarry = false;
        this.flagInterupcao = false;
        this.flagOverFlow = false;
        this.flagParidade = false;
        this.flagZero = false;
        this.flagZero = false;

    }

    private int contaBits(int numero) {
        int bits_necesarios = 0;
        while (numero > 0) {
            bits_necesarios++;
            numero = numero >> 1; // Desplazo bits (división por 2)
        }
        return bits_necesarios;
    }
    
    public int getAX(){
        return AX;
    }
    public int getDX(){
        return DX;
    }
    public boolean getCarry(){
        return flagCarry; 
    }
    public boolean getZero(){
         return flagZero; 
    }
    public boolean getOverflow(){
        return flagOverFlow; 
    }
    
   
  
    

//    public static void main(String[] args) throws FileNotFoundException, IOException {
//        Carregador texte = new Carregador();
//
//        texte.carrega("entrada.txt", 1); // OPÇÃO 1  EXECUTA TUDOOO OPÇÃO 2 EXECUTA PASSO A PASSO USA A TECLA ENTER PRA PASSA
//
//    }

}
