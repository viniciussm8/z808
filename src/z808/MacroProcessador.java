/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z808;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author rsilveira 11108713
 */
public class MacroProcessador {

    boolean flagDeChamada = false;
    boolean flagDeMacro = false;
    boolean flagDeEndm = false;
    boolean flagTeste = false;
    boolean modoDeOperacaoMd, modoDeOperacaoMe;
    String seqDeCarLinha, Instrucao;
    int i=0;
   
    ArrayList<String> tabelaDeDefinicaoDeMacros = new ArrayList<>();
    ArrayList<String> tabelaDeNomesDeMacros = new ArrayList<>();
    String[] recebeSplit = new String[10];
    String[] quebraLinha = new String[10];
    String[] resposta = new String[10];
    String[] listaDeParametrosReais = new String[10];
    String[] teste=new String[10];

    public void processaMacro(String Nome) throws FileNotFoundException, IOException {
        File arquivoDeEntrada = new File(Nome);
        File apaga = new File("codigoexpandido.txt");
        if (apaga.exists()){
             apaga.delete();
          } 
        File arquivoDeSaida = new File("codigoexpandido.txt");
        arquivoDeSaida.createNewFile();
       // System.out.println(arquivoDeSaida.canWrite());
        //System.out.println(arquivoDeEntrada.toString());
        //System.out.println(arquivoDeEntrada.length());
        modoDeOperacaoMd = false;
        modoDeOperacaoMe = false;

        try (BufferedReader inFile = new BufferedReader(new FileReader(arquivoDeEntrada))) {
            try (BufferedWriter outFile = new BufferedWriter(new FileWriter(arquivoDeSaida, true))) {
                String inputLine;
                //outFile.write("");
                while ((inputLine = inFile.readLine()) != null) {  //enquanto nao acabar o arquivo, le linha por linha
                 
                    Instrucao = procuraInstrucao(inputLine); //procura por instrução na linha
                    /*
                    if (recebeSplit[0] !=null){
                            if(inputLine.contains(recebeSplit[0])){
                            System.out.println("AQUIIII " + recebeSplit[0]);
                                                        
                                    outFile.write(" ");  //escreve no arquivo de saida
                                                             
                                   
                                }
                            else{
                                     outFile.write(inputLine);  //escreve no arquivo de saida
                                     outFile.newLine(); //nova linha no arquivo de saida
                                     
                                     
                                }
                    }else{
                           outFile.write(inputLine);  //escreve no arquivo de saida
                           outFile.newLine(); //nova linha no arquivo de saida  
                             }
                    */
                    if(flagTeste==false){
                            outFile.write(inputLine);  //escreve no arquivo de saida
                            outFile.newLine(); //nova linha no arquivo de saida  
                    }
                    if (modoDeOperacaoMd == true) {
                        
                        if ("ENDM".equals(Instrucao)) {
                            tabelaDeDefinicaoDeMacros.add("ENDM");
                            modoDeOperacaoMd = false;
                        } else {
                            
                            escreveNatabela(substituiParametrosPorPosicoes(inputLine));

                        }
                    } else {
                        switch (Instrucao) {   //retorno de instrução
                            case "CHAMADA DE MACRO":
                                modoDeOperacaoMe = true;
                                //System.out.println("Chamada de Macro");
                                listaDeParametrosReais = quebraLinha(inputLine); //retorna os parametros
                                //System.out.println("Lista de Parametros Reais: " + Arrays.toString(listaDeParametrosReais));
                                //PREPARA listaDeParametrosReais
                                break;
                            case "MACRO":
                                modoDeOperacaoMd = true;

                                // alocaMacro(inputLine);
                                break;
                            case "ENDM":
                                modoDeOperacaoMe = false;
                                //DESCARTA listaDeParametrosReais
                                break;
                            default:
                                if (modoDeOperacaoMe == true) {
                                  
                                    outFile.newLine();
                                    
                                }
                                break;
                        }//FIM DO SWITCH
                        if (modoDeOperacaoMe == true) {
                          
                            outFile.newLine();
                            String troca;
                            for (int per = 0; per < tabelaDeDefinicaoDeMacros.size(); per++) {

                                if (tabelaDeDefinicaoDeMacros.get(per).contains(listaDeParametrosReais[0])) {
                                    for (int esc = per+1;; esc++) {
                                        if (tabelaDeDefinicaoDeMacros.get(esc).contains("ENDM")) {
                                            break;
                                        }
                                       // System.out.println("Se tem é porque achei");
                                       
                                        troca = tabelaDeDefinicaoDeMacros.get(esc);
                                        if (troca.contains("1")) {
                                            troca = troca.replace("1", listaDeParametrosReais[1]);
                                        }
                                        if (troca.contains("2")) {
                                            troca = troca.replace("2", listaDeParametrosReais[2]);
                                        }
                                        
                                        outFile.write(troca);
                                        outFile.newLine();
                                        
                                    }
                                }
                                modoDeOperacaoMe = false;

                            }
                            
                        } 
                    }
                }//FIM DO WHILE
            }
            
        } catch (Exception e) {
            
        }

    }

    private String procuraInstrucao(String inputLine) {
        //PROCURA EM UMA LINHA DE TEXTO UMA INSTRUÇAO 

        String match = "NOP";

        if (inputLine.contains("MACRO")) {
            
            tabelaDeNomesDeMacros.add(inputLine); //adiciona linha a tabela de macros
            descobreOsParametros(inputLine); //chama a fun��o descobre parametros
            tabelaDeDefinicaoDeMacros.add(inputLine);
            match = "MACRO";
            flagDeMacro = true;
            flagDeChamada = false;
            flagDeEndm = false;
            flagTeste = true;
        }
        if (inputLine.contains("ENDM")) {
            
            match = "ENDM";
            flagDeMacro = false;
            flagDeChamada = false;
            flagDeEndm = true;
        }
        if (inputLine.contains("Inicio")) {
            
            
            flagTeste = false;
           
        }
        if (modoDeOperacaoMd == false) {
            

            if (verificaSeInstrucaoEhMacro(inputLine)) {

                flagDeMacro = false;
                flagDeChamada = true;
                flagDeEndm = false;
             
                return "CHAMADA DE MACRO";
            }
        }
        if (flagDeMacro == true) {
            return "MACRO";
        }
        if (flagDeEndm == true) {
            return "ENDM";
        }
        return match;

    }

    private boolean verificaSeInstrucaoEhMacro(String verificaLinha) {
        
        String[] recebeLinhaQuebrada = quebraLinha(verificaLinha);

        for (int i = 0; i < tabelaDeNomesDeMacros.size(); i++) {
            for (int j = 0; j < recebeLinhaQuebrada.length; j++) {
                if (tabelaDeNomesDeMacros.get(i).contains(recebeLinhaQuebrada[j]) && !recebeLinhaQuebrada[j].contains(" MACRO") && !verificaLinha.contains(" MACRO")) {
                   
                    return true;
                }
            }
        }
        //VERIFICA NA TABELA DE NOMES DE MACROS
        return false;
    }

    

    private void escreveNatabela(String inputLine) {
        
        tabelaDeDefinicaoDeMacros.add(inputLine);
    }

    private String substituiParametrosPorPosicoes(String inputLine) {
       
        quebraLinha = inputLine.split("[,||' ']");

        String resultado = new String();
        String inteiroToString;
        for (int percorreLinha = 0; percorreLinha < quebraLinha.length; percorreLinha++) {
            boolean flagDeMatch = false;
            int aux = 0;
            for (int percorreRecebeSplit = 2; percorreRecebeSplit < recebeSplit.length; percorreRecebeSplit++) {
               
                if (quebraLinha[percorreLinha].equals(recebeSplit[percorreRecebeSplit])) {
                
                    aux = percorreRecebeSplit - 1;
                    inteiroToString = " " + aux + ",";
                   
                    resultado = resultado.concat(inteiroToString + "");
                    flagDeMatch = true;
                } 
                
            }
            if (flagDeMatch == false) {
                resultado = resultado.concat(" " + quebraLinha[percorreLinha] + ",");
                resultado = resultado.replace("mov,", "mov");
                resultado = resultado.replace("add,", "add");
                resultado = resultado.replace("pop,", "pop");
                resultado = resultado.replace("push,", "push");
               
            }
        }
        return resultado;
    }

    public void descobreOsParametros(String inputLine) {
        recebeSplit = inputLine.split("[,||' ']");
        
       

    }

    public String[] quebraLinha(String entrada) {
        resposta = entrada.split("[,||' ']");

        for (int i = 0; i < resposta.length; i++) {
           
            resposta[i] = resposta[i].trim();
            
        }

        return resposta;
    }

}
