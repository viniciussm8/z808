/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z808;

import java.io.BufferedReader; 
import java.io.BufferedWriter; 
import java.io.FileReader; 
import java.io.FileWriter; 
import java.io.IOException; 
import java.util.ArrayList;
import java.util.Scanner;


// Sera feito um montador de dois passos, com o código fonte saido do processador de macros.
/**
 *
 * @author Lucas
 */
public class Montador {
    private boolean okPrimeiroParametro;
    private boolean okSegundoParametro;
    private boolean ocorreuErro;
    private ArrayList<Simbolos> tabelaDeSimbolos;
    
    // Construtor da Classe
    public Montador(){
        this.tabelaDeSimbolos = new ArrayList<Simbolos>();
        this.okPrimeiroParametro = false;
        this.okSegundoParametro = false;
        this.ocorreuErro = false;
        
    }
    
    public void Montagem(String nomeArquivo) throws IOException{
        int pointerLine=0;
        int i=0, j = 0;
        BufferedReader buffRead = new BufferedReader(new FileReader(nomeArquivo));    
        BufferedWriter buffWrite = new BufferedWriter(new FileWriter("codeObjeto.txt"));
        String linha = ""; 
        linha = buffRead.readLine();
        while (linha!=null) { 
            linha = linha.replaceFirst("\t","");
            linha = linha.replaceAll("\t"," ");
        // Verificara cada instrucao, levando em consideracao todos os parametros escritos, e transoformara para codigo
        // de maquina.
            if (linha.contains("Dados")){
                linha = buffRead.readLine();
                
            }
            else if (linha.contains("SEGMENT")){
                linha = buffRead.readLine();
            }
            
            else if (linha.contains("ASSUME")){
                linha = buffRead.readLine();
            }
            
            else if (linha.contains("Inicio")){
                linha = buffRead.readLine();
            }
            else if (linha.contains("END")){
                linha = buffRead.readLine();
            }
            
            
            //INSTRUCOES DO CODE PROPRIAMENTEDITO
            else if (linha.contains("add")){
                pointerLine++;
                String aux;
                String[] aux2;
                String primeiroParametro="", segundoParametro="";
                String opCode="";
                boolean auxLogic=true;
                aux = linha.replaceAll(" ",":");
                aux = aux.replaceAll(",",":");
                aux2 = aux.split(":");
                if("AX".equals(aux2[1])){
                    opCode = "03";
                    primeiroParametro = "AX";
                    this.okPrimeiroParametro = true;
                    pointerLine++;
                }
                else if("DX".equals(aux2[1])){
                    pointerLine++;
                    opCode = "03";
                    primeiroParametro = "DX";
                    this.okPrimeiroParametro = true;
                }
                else{
                    for(i=0;i<this.tabelaDeSimbolos.size();i++){
                        if(this.tabelaDeSimbolos.get(i).compareSimbolo(aux2[1])){
                            pointerLine++;
                            opCode = "05";
                            primeiroParametro = this.tabelaDeSimbolos.get(i).getEndereco();
                            this.okPrimeiroParametro = true;
                        }
                    }
                }
                
                if(this.verificaPrimeiroParametro()){
                    if("AX".equals(aux2[2])){
                        opCode = "03";
                        pointerLine++;
                        segundoParametro = "AX";
                        this.okSegundoParametro = true;
                    }
                    else if("DX".equals(aux2[2])){
                        opCode = "03";
                        segundoParametro = "DX";
                        pointerLine++;
                        this.okSegundoParametro = true;
                    }
                    else{
                        for(i=0;i<this.tabelaDeSimbolos.size();i++){
                            if(this.tabelaDeSimbolos.get(i).compareSimbolo(aux2[2])){
                                opCode = "05";
                                pointerLine++;
                                segundoParametro = this.tabelaDeSimbolos.get(i).getEndereco();
                                this.okSegundoParametro = true;
                            }
                        }
                    }
                }
                
                else{
                    this.ocorreuErro = true;
                }
                buffWrite.append(opCode+" "+primeiroParametro+" "+segundoParametro+"\r\n");
                linha = buffRead.readLine();
            }
            
            else if (linha.contains("sub")){
                pointerLine++;
                String aux;
                String[] aux2;
                String primeiroParametro="", segundoParametro="";
                String opCode="";
                boolean auxLogic=true;
                aux = linha.replaceAll(" ",":");
                aux = aux.replaceAll(",",":");
                aux2 = aux.split(":");
                if("AX".equals(aux2[1])){
                    opCode = "2B";
                    primeiroParametro = "AX";
                    this.okPrimeiroParametro = true;
                    pointerLine++;
                }
                else if("DX".equals(aux2[1])){
                    pointerLine++;
                    opCode = "2B";
                    primeiroParametro = "DX";
                    this.okPrimeiroParametro = true;
                }
                else{
                    for(i=0;i<this.tabelaDeSimbolos.size();i++){
                        if(this.tabelaDeSimbolos.get(i).compareSimbolo(aux2[1])){
                            pointerLine++;
                            opCode = "2D";
                            primeiroParametro = this.tabelaDeSimbolos.get(i).getEndereco();
                            this.okPrimeiroParametro = true;
                        }
                    }
                }
                
                if(this.verificaPrimeiroParametro()){
                    if("AX".equals(aux2[2])){
                        opCode = "2B";
                        pointerLine++;
                        segundoParametro = "AX";
                        this.okSegundoParametro = true;
                    }
                    else if("DX".equals(aux2[2])){
                        opCode = "2B";
                        segundoParametro = "DX";
                        pointerLine++;
                        this.okSegundoParametro = true;
                    }
                    else{
                        for(i=0;i<this.tabelaDeSimbolos.size();i++){
                            if(this.tabelaDeSimbolos.get(i).compareSimbolo(aux2[2])){
                                opCode = "2D";
                                pointerLine++;
                                segundoParametro = this.tabelaDeSimbolos.get(i).getEndereco();
                                this.okSegundoParametro = true;
                            }
                        }
                    }
                }
                
                else{
                    this.ocorreuErro = true;
                }
                buffWrite.append(opCode+" "+primeiroParametro+" "+segundoParametro+"\r\n");
                linha = buffRead.readLine();
            }
            
            else if (linha.contains("cmp")){
                pointerLine++;
                String aux;
                String[] aux2;
                String primeiroParametro="", segundoParametro="";
                String opCode="";
                aux = linha.replaceAll(" ",":");
                aux = aux.replaceAll(",",":");
                aux2 = aux.split(":");
                if("AX".equals(aux2[1])){
                    opCode = "3B";
                    primeiroParametro = "AX";
                    this.okPrimeiroParametro = true;
                    pointerLine++;
                }
                else if("DX".equals(aux2[1])){
                    pointerLine++;
                    opCode = "3B";
                    primeiroParametro = "DX";
                    this.okPrimeiroParametro = true;
                }
                else{
                    for(i=0;i<this.tabelaDeSimbolos.size();i++){
                        if(this.tabelaDeSimbolos.get(i).compareSimbolo(aux2[1])){
                            pointerLine++;
                            opCode = "3D";
                            primeiroParametro = this.tabelaDeSimbolos.get(i).getEndereco();
                            this.okPrimeiroParametro = true;
                        }
                    }
                }
                
                if(this.verificaPrimeiroParametro()){
                    if("AX".equals(aux2[2])){
                        opCode = "3B";
                        pointerLine++;
                        segundoParametro = "AX";
                        this.okSegundoParametro = true;
                    }
                    else if("DX".equals(aux2[2])){
                        opCode = "3B";
                        segundoParametro = "DX";
                        pointerLine++;
                        this.okSegundoParametro = true;
                    }
                    else{
                        for(i=0;i<this.tabelaDeSimbolos.size();i++){
                            if(this.tabelaDeSimbolos.get(i).compareSimbolo(aux2[2])){
                                opCode = "3D";
                                pointerLine++;
                                segundoParametro = this.tabelaDeSimbolos.get(i).getEndereco();
                                this.okSegundoParametro = true;
                            }
                        }
                    }
                }
                
                else{
                    this.ocorreuErro = true;
                }
                buffWrite.append(opCode+" "+primeiroParametro+" "+segundoParametro+"\r\n");
                linha = buffRead.readLine();
            }
            
            
            //LOGICOS
            else if (linha.contains("and")){
                pointerLine++;
                String aux;
                String[] aux2;
                String primeiroParametro="", segundoParametro="";
                String opCode="";
                aux = linha.replaceAll(" ",":");
                aux = aux.replaceAll(",",":");
                aux2 = aux.split(":");
                if("AX".equals(aux2[1])){
                    opCode = "23";
                    primeiroParametro = "AX";
                    this.okPrimeiroParametro = true;
                    pointerLine++;
                }
                else if("DX".equals(aux2[1])){
                    pointerLine++;
                    opCode = "23";
                    primeiroParametro = "DX";
                    this.okPrimeiroParametro = true;
                }
                else{
                    for(i=0;i<this.tabelaDeSimbolos.size();i++){
                        if(this.tabelaDeSimbolos.get(i).compareSimbolo(aux2[1])){
                            pointerLine++;
                            opCode = "25";
                            primeiroParametro = this.tabelaDeSimbolos.get(i).getEndereco();
                            this.okPrimeiroParametro = true;
                        }
                    }
                }
                
                if(this.verificaPrimeiroParametro()){
                    if("AX".equals(aux2[2])){
                        opCode = "23";
                        pointerLine++;
                        segundoParametro = "AX";
                        this.okSegundoParametro = true;
                    }
                    else if("DX".equals(aux2[2])){
                        opCode = "23";
                        segundoParametro = "DX";
                        pointerLine++;
                        this.okSegundoParametro = true;
                    }
                    else{
                        for(i=0;i<this.tabelaDeSimbolos.size();i++){
                            if(this.tabelaDeSimbolos.get(i).compareSimbolo(aux2[2])){
                                opCode = "25";
                                pointerLine++;
                                segundoParametro = this.tabelaDeSimbolos.get(i).getEndereco();
                                this.okSegundoParametro = true;
                            }
                        }
                    }
                }
                
                else{
                    this.ocorreuErro = true;
                }
                buffWrite.append(opCode+" "+primeiroParametro+" "+segundoParametro+"\r\n");
                linha = buffRead.readLine();
            }
            
            else if (linha.contains("or")){
                pointerLine++;
                String aux;
                String[] aux2;
                String primeiroParametro="", segundoParametro="";
                String opCode="";
                aux = linha.replaceAll(" ",":");
                aux = aux.replaceAll(",",":");
                aux2 = aux.split(":");
                if("AX".equals(aux2[1])){
                    opCode = "0B";
                    primeiroParametro = "AX";
                    this.okPrimeiroParametro = true;
                    pointerLine++;
                }
                else if("DX".equals(aux2[1])){
                    pointerLine++;
                    opCode = "0B";
                    primeiroParametro = "DX";
                    this.okPrimeiroParametro = true;
                }
                else{
                    for(i=0;i<this.tabelaDeSimbolos.size();i++){
                        if(this.tabelaDeSimbolos.get(i).compareSimbolo(aux2[1])){
                            pointerLine++;
                            opCode = "0D";
                            primeiroParametro = this.tabelaDeSimbolos.get(i).getEndereco();
                            this.okPrimeiroParametro = true;
                        }
                    }
                }
                
                if(this.verificaPrimeiroParametro()){
                    if("AX".equals(aux2[2])){
                        opCode = "0B";
                        pointerLine++;
                        segundoParametro = "AX";
                        this.okSegundoParametro = true;
                    }
                    else if("DX".equals(aux2[2])){
                        opCode = "0B";
                        segundoParametro = "DX";
                        pointerLine++;
                        this.okSegundoParametro = true;
                    }
                    else{
                        for(i=0;i<this.tabelaDeSimbolos.size();i++){
                            if(this.tabelaDeSimbolos.get(i).compareSimbolo(aux2[2])){
                                opCode = "0D";
                                pointerLine++;
                                segundoParametro = this.tabelaDeSimbolos.get(i).getEndereco();
                                this.okSegundoParametro = true;
                            }
                        }
                    }
                }
                
                else{
                    this.ocorreuErro = true;
                }
                buffWrite.append(opCode+" "+primeiroParametro+" "+segundoParametro+"\r\n");
                linha = buffRead.readLine();
            }
            
            else if (linha.contains("xor")){
                pointerLine++;
                String aux;
                String[] aux2;
                String primeiroParametro="", segundoParametro="";
                String opCode="";
                aux = linha.replaceAll(" ",":");
                aux = aux.replaceAll(",",":");
                aux2 = aux.split(":");
                if("AX".equals(aux2[1])){
                    opCode = "33";
                    primeiroParametro = "AX";
                    this.okPrimeiroParametro = true;
                    pointerLine++;
                }
                else if("DX".equals(aux2[1])){
                    pointerLine++;
                    opCode = "33";
                    primeiroParametro = "DX";
                    this.okPrimeiroParametro = true;
                }
                else{
                    for(i=0;i<this.tabelaDeSimbolos.size();i++){
                        if(this.tabelaDeSimbolos.get(i).compareSimbolo(aux2[1])){
                            pointerLine++;
                            opCode = "35";
                            primeiroParametro = this.tabelaDeSimbolos.get(i).getEndereco();
                            this.okPrimeiroParametro = true;
                        }
                    }
                }
                
                if(this.verificaPrimeiroParametro()){
                    if("AX".equals(aux2[2])){
                        opCode = "33";
                        pointerLine++;
                        segundoParametro = "AX";
                        this.okSegundoParametro = true;
                    }
                    else if("DX".equals(aux2[2])){
                        opCode = "33";
                        segundoParametro = "DX";
                        pointerLine++;
                        this.okSegundoParametro = true;
                    }
                    else{
                        for(i=0;i<this.tabelaDeSimbolos.size();i++){
                            if(this.tabelaDeSimbolos.get(i).compareSimbolo(aux2[2])){
                                opCode = "35";
                                pointerLine++;
                                segundoParametro = this.tabelaDeSimbolos.get(i).getEndereco();
                                this.okSegundoParametro = true;
                            }
                        }
                    }
                }
                
                else{
                    this.ocorreuErro = true;
                }
                buffWrite.append(opCode+" "+primeiroParametro+" "+segundoParametro+"\r\n");
                linha = buffRead.readLine();
            }
            
            else if (linha.contains("mov")){
                pointerLine++;
                String aux;
                String[] aux2;
                String primeiroParametro="", segundoParametro="";
                String opCode="";
                aux = linha.replaceAll(" ",":");
                aux = aux.replaceAll(",",":");
                aux2 = aux.split(":");
                if("AX".equals(aux2[1])){
                    opCode = "8B";
                    primeiroParametro = "AX";
                    this.okPrimeiroParametro = true;
                    pointerLine++;
                }
                else if("DX".equals(aux2[1])){
                    pointerLine++;
                    opCode = "8B";
                    primeiroParametro = "DX";
                    this.okPrimeiroParametro = true;
                }
                else{
                    for(i=0;i<this.tabelaDeSimbolos.size();i++){
                        if(this.tabelaDeSimbolos.get(i).compareSimbolo(aux2[1])){
                            pointerLine++;
                            opCode = "B8";
                            primeiroParametro = this.tabelaDeSimbolos.get(i).getEndereco();
                            this.okPrimeiroParametro = true;
                        }
                    }
                }
                
                if(this.verificaPrimeiroParametro()){
                    if("AX".equals(aux2[2])){
                        opCode = "8B";
                        pointerLine++;
                        segundoParametro = "AX";
                        this.okSegundoParametro = true;
                    }
                    else if("DX".equals(aux2[2])){
                        opCode = "8B";
                        segundoParametro = "DX";
                        pointerLine++;
                        this.okSegundoParametro = true;
                    }
                    else{
                        for(i=0;i<this.tabelaDeSimbolos.size();i++){
                            if(this.tabelaDeSimbolos.get(i).compareSimbolo(aux2[2])){
                                opCode = "B8";
                                pointerLine++;
                                segundoParametro = this.tabelaDeSimbolos.get(i).getEndereco();
                                this.okSegundoParametro = true;
                            }
                        }
                    }
                }
                
                else{
                    this.ocorreuErro = true;
                }
                buffWrite.append(opCode+" "+primeiroParametro+" "+segundoParametro+"\r\n");
                linha = buffRead.readLine();
            }
            
            else if (linha.contains("not")){
                pointerLine++;
                String aux;
                String[] aux2;
                String primeiroParametro="";
                String opCode="";
                aux = linha.replaceAll(" ",":");
                aux = aux.replaceAll(",",":");
                aux2 = aux.split(":");
                if("AX".equals(aux2[1])){
                    opCode = "F7";
                    primeiroParametro = "AX";
                    this.okPrimeiroParametro = true;
                    pointerLine++;
                }
                else if("DX".equals(aux2[1])){
                    pointerLine++;
                    opCode = "F7";
                    primeiroParametro = "DX";
                    this.okPrimeiroParametro = true;
                }
                else{
                    for(i=0;i<this.tabelaDeSimbolos.size();i++){
                        if(this.tabelaDeSimbolos.get(i).compareSimbolo(aux2[1])){
                            pointerLine++;
                            opCode = "F7";
                            primeiroParametro = this.tabelaDeSimbolos.get(i).getEndereco();
                            this.okPrimeiroParametro = true;
                        }
                    }
                }
           
                buffWrite.append(opCode+" "+primeiroParametro+"\r\n");
                linha = buffRead.readLine();
            }
            
            else if (linha.contains("div")){
                pointerLine++;
                String aux;
                String[] aux2;
                String primeiroParametro="";
                String opCode="";
                aux = linha.replaceAll(" ",":");
                aux2 = aux.split(":");
                if("AX".equals(aux2[1])){
                    opCode = "F6";
                    primeiroParametro = "AX";
                    this.okPrimeiroParametro = true;
                    pointerLine++;
                }
                else if("DX".equals(aux2[1])){
                    pointerLine++;
                    opCode = "F6";
                    primeiroParametro = "DX";
                    this.okPrimeiroParametro = true;
                }
                else{
                    for(i=0;i<this.tabelaDeSimbolos.size();i++){
                        if(this.tabelaDeSimbolos.get(i).compareSimbolo(aux2[1])){
                            pointerLine++;
                            opCode = "F6";
                            primeiroParametro = this.tabelaDeSimbolos.get(i).getEndereco();
                            this.okPrimeiroParametro = true;
                        }
                    }
                }
           
                buffWrite.append(opCode+" "+primeiroParametro+"\r\n");
                linha = buffRead.readLine();
            }
            
            else if (linha.contains("mul")){
                pointerLine++;
                String aux;
                String[] aux2;
                String primeiroParametro="";
                String opCode="";
                aux = linha.replaceAll(" ",":");
                aux = aux.replaceAll(",",":");
                aux2 = aux.split(":");
                if("AX".equals(aux2[1])){
                    opCode = "E6";
                    primeiroParametro = "AX";
                    this.okPrimeiroParametro = true;
                    pointerLine++;
                }
                else if("DX".equals(aux2[1])){
                    pointerLine++;
                    opCode = "E6";
                    primeiroParametro = "DX";
                    this.okPrimeiroParametro = true;
                }
                else{
                    for(i=0;i<this.tabelaDeSimbolos.size();i++){
                        if(this.tabelaDeSimbolos.get(i).compareSimbolo(aux2[1])){
                            pointerLine++;
                            opCode = "E6";
                            primeiroParametro = this.tabelaDeSimbolos.get(i).getEndereco();
                            this.okPrimeiroParametro = true;
                        }
                    }
                }
           
                buffWrite.append(opCode+" "+primeiroParametro+"\r\n");
                linha = buffRead.readLine();
            }
            
            else if (linha.contains("jmp")){
                pointerLine++;
                String aux;
                String[] aux2;
                String primeiroParametro="";
                String opCode="";
                aux = linha.replaceAll(" ",":");
                aux = aux.replaceAll(",",":");
                aux2 = aux.split(":");
                if("AX".equals(aux2[1])){
                    opCode = "EB";
                    primeiroParametro = "AX";
                    this.okPrimeiroParametro = true;
                    pointerLine++;
                }
                else if("DX".equals(aux2[1])){
                    pointerLine++;
                    opCode = "EB";
                    primeiroParametro = "DX";
                    this.okPrimeiroParametro = true;
                }
                else{
                    for(i=0;i<this.tabelaDeSimbolos.size();i++){
                        if(this.tabelaDeSimbolos.get(i).compareSimbolo(aux2[1])){
                            pointerLine++;
                            opCode = "EB";
                            primeiroParametro = this.tabelaDeSimbolos.get(i).getEndereco();
                            this.okPrimeiroParametro = true;
                        }
                    }
                }
           
                buffWrite.append(opCode+" "+primeiroParametro+"\r\n");
                linha = buffRead.readLine();
            }
            
            else if (linha.contains("je")){
                pointerLine++;
                String aux;
                String[] aux2;
                String primeiroParametro="";
                String opCode="";
                aux = linha.replaceAll(" ",":");
                aux = aux.replaceAll(",",":");
                aux2 = aux.split(":");
                if("AX".equals(aux2[1])){
                    opCode = "74";
                    primeiroParametro = "AX";
                    this.okPrimeiroParametro = true;
                    pointerLine++;
                }
                else if("DX".equals(aux2[1])){
                    pointerLine++;
                    opCode = "74";
                    primeiroParametro = "DX";
                    this.okPrimeiroParametro = true;
                }
                else{
                    for(i=0;i<this.tabelaDeSimbolos.size();i++){
                        if(this.tabelaDeSimbolos.get(i).compareSimbolo(aux2[1])){
                            pointerLine++;
                            opCode = "74";
                            primeiroParametro = this.tabelaDeSimbolos.get(i).getEndereco();
                            this.okPrimeiroParametro = true;
                        }
                    }
                }
           
                buffWrite.append(opCode+" "+primeiroParametro+"\r\n");
                linha = buffRead.readLine();
            }
            
            else if (linha.contains("jnz")){
                pointerLine++;
                String aux;
                String[] aux2;
                String primeiroParametro="";
                String opCode="";
                aux = linha.replaceAll(" ",":");
                aux = aux.replaceAll(",",":");
                aux2 = aux.split(":");
                if("AX".equals(aux2[1])){
                    opCode = "75";
                    primeiroParametro = "AX";
                    this.okPrimeiroParametro = true;
                    pointerLine++;
                }
                else if("DX".equals(aux2[1])){
                    pointerLine++;
                    opCode = "75";
                    primeiroParametro = "DX";
                    this.okPrimeiroParametro = true;
                }
                else{
                    for(i=0;i<this.tabelaDeSimbolos.size();i++){
                        if(this.tabelaDeSimbolos.get(i).compareSimbolo(aux2[1])){
                            pointerLine++;
                            opCode = "75";
                            primeiroParametro = this.tabelaDeSimbolos.get(i).getEndereco();
                            this.okPrimeiroParametro = true;
                        }
                    }
                }
           
                buffWrite.append(opCode+" "+primeiroParametro+"\r\n");
                linha = buffRead.readLine();
            }
            
            else if (linha.contains("jp")){
                pointerLine++;
                String aux;
                String[] aux2;
                String primeiroParametro="";
                String opCode="";
                aux = linha.replaceAll(" ",":");
                aux = aux.replaceAll(",",":");
                aux2 = aux.split(":");
                if("AX".equals(aux2[1])){
                    opCode = "7A";
                    primeiroParametro = "AX";
                    this.okPrimeiroParametro = true;
                    pointerLine++;
                }
                else if("DX".equals(aux2[1])){
                    pointerLine++;
                    opCode = "7A";
                    primeiroParametro = "DX";
                    this.okPrimeiroParametro = true;
                }
                else{
                    for(i=0;i<this.tabelaDeSimbolos.size();i++){
                        if(this.tabelaDeSimbolos.get(i).compareSimbolo(aux2[1])){
                            pointerLine++;
                            opCode = "7A";
                            primeiroParametro = this.tabelaDeSimbolos.get(i).getEndereco();
                            this.okPrimeiroParametro = true;
                        }
                    }
                }
           
                buffWrite.append(opCode+" "+primeiroParametro+"\r\n");
                linha = buffRead.readLine();
            }
            
            else if (linha.contains("pop")){
                pointerLine++;
                String aux;
                String[] aux2;
                String primeiroParametro="";
                String opCode="";
                aux = linha.replaceAll(" ",":");
                aux = aux.replaceAll(",",":");
                aux2 = aux.split(":");
                if("AX".equals(aux2[1])){
                    opCode = "58";
                    primeiroParametro = "AX";
                    this.okPrimeiroParametro = true;
                    pointerLine++;
                }
                else if("DX".equals(aux2[1])){
                    pointerLine++;
                    opCode = "58";
                    primeiroParametro = "DX";
                    this.okPrimeiroParametro = true;
                }
                else{
                    for(i=0;i<this.tabelaDeSimbolos.size();i++){
                        if(this.tabelaDeSimbolos.get(i).compareSimbolo(aux2[1])){
                            pointerLine++;
                            opCode = "58";
                            primeiroParametro = this.tabelaDeSimbolos.get(i).getEndereco();
                            this.okPrimeiroParametro = true;
                        }
                    }
                }
           
                buffWrite.append(opCode+" "+primeiroParametro+"\r\n");
                linha = buffRead.readLine();
            }
            
            else if (linha.contains("push")){
                pointerLine++;
                String aux;
                String[] aux2;
                String primeiroParametro="";
                String opCode="";
                aux = linha.replaceAll(" ",":");
                aux = aux.replaceAll(",",":");
                aux2 = aux.split(":");
                if("AX".equals(aux2[1])){
                    opCode = "50";
                    primeiroParametro = "AX";
                    this.okPrimeiroParametro = true;
                    pointerLine++;
                }
                else if("DX".equals(aux2[1])){
                    pointerLine++;
                    opCode = "50";
                    primeiroParametro = "DX";
                    this.okPrimeiroParametro = true;
                }
                else{
                    for(i=0;i<this.tabelaDeSimbolos.size();i++){
                        if(this.tabelaDeSimbolos.get(i).compareSimbolo(aux2[1])){
                            pointerLine++;
                            opCode = "50";
                            primeiroParametro = this.tabelaDeSimbolos.get(i).getEndereco();
                            this.okPrimeiroParametro = true;
                        }
                    }
                }
                
           
                buffWrite.append(opCode+" "+primeiroParametro+"\r\n");
                linha = buffRead.readLine();
            }
            
            else{
                
                if(linha.length()>1){    
                // Condicao caso seja um dado
                String novaVariavel="";
                String[] aux;
                novaVariavel = linha.replaceAll(" ",":");
                aux = novaVariavel.split(":");
                novaVariavel = aux[0];
                Simbolos novoSimbolo = new Simbolos(novaVariavel,pointerLine);
                this.tabelaDeSimbolos.add(novoSimbolo);
                buffWrite.append(pointerLine+" "+linha.substring(j+7)+"\r\n");
                pointerLine++;
                pointerLine++;
                }
                linha = buffRead.readLine();
            }
                
                
        }
            
            
            //buffWrite.append(linha+"\n");
             buffRead.close();
            buffWrite.close();
            //linha = buffRead.readLine();
            
    }
    
    public boolean verificaPrimeiroParametro(){
        return this.okPrimeiroParametro;
    }
    
    
    public boolean verificaSegundoParametro(){
        return this.okSegundoParametro;
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        Montador a = new Montador();
        a.Montagem("codigoexpandido.txt");
        //a.Montagem("a.txt");
    }
    
}
